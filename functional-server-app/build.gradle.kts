version = "0.1.0"

plugins {
    id("Sockets.java-application-conventions")
    id("com.google.cloud.tools.jib") version "3.2.1"
}

dependencies {
    implementation("org.apache.httpcomponents:httpclient:4.5.13")
}

application {
    // Define the main class for the application.
    mainClass.set("app.App")
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "app.App"
    }
}

val registry = System.getenv("DOCKER_REGISTRY")
val build_branch = System.getenv("CI_COMMIT_BRANCH")
val docker_login = System.getenv("DOCKER_NAME")
val docker_password = System.getenv("DOCKER_TOKEN")

jib {
    from {
        image = "shakhtarovivan/baseimage:jre11"
    }
    to {
        image = "$registry/test-java-app"
        tags = setOf("$build_branch", "$version")
        auth {
            username = "$docker_login"
            password = "$docker_password"
        }
    }
    container{
        mainClass = "app.App"
        user = "1000"
    }
}